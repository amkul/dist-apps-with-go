#!/usr/bin/env bash
set +e
go get -u github.com/streadway/amqp
go install github.com/streadway/amqp
