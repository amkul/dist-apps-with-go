// dto stands for data transfer objects.
// This holds definition for any type that we want to send through message broker

package dto

import (
	"encoding/gob" // used for data encoding
	"time"
)

type SensorMessage struct {
	Name      string
	Value     float64
	Timestamp time.Time
}

func init() {
	gob.Register(SensorMessage{})
}
